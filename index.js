document.addEventListener("DOMContentLoaded", () => {
    var post = document.getElementById("content")
    var product = document.getElementById("product")
    _u = _.noConflict();
    function fetchData(url) {
        return new Promise((resolve, reject) => {
            fetch(url)
            .then(res => res.json())
            .then(res => resolve(res))
        })
    }

    function prepareData(users, posts) {
        var newObj = []
        var userLimit = 5
 
        for(let i=0;i<userLimit;i++ ){
            let obj = {
                userId: users[i].id,
                username: users[i].username,
                posts: []
            }
            for(let y=0;y<posts.length;y++) {
                if(users[i].id == posts[y].userId){
                    obj.posts.push({
                        title: posts[y].title,
                        body: posts[y].body
                    }) 
                }
            }
            newObj.push(obj)
        }
        return newObj
    }

    function renderUsers(users) {
        let html = `` 
        users.map((el,idx) => {
            html += `<div key=${idx} id="post">
                        <div class="user">
                            <h2 key=${idx}>${el.username} (${el.posts.length})</h2> 
                        </div>
                        <div id="post-list" class="none" key=${idx}>
                            <div class="posts">
                            ${
                                el.posts.map( i => (
                                    `<h3>Title : ${i.title}</h3>
                                     <span>
                                        ${i.body}
                                     </span>
                                    `
                                )).join("")
                            }
                            </div>
                        </div>
                    </div>
            `
        })
        post.innerHTML = html
        //console.log(users)
    }

    
    function prepareProducts(products) {
        let groupByCategory = _u.chain(products).groupBy("category").map((value, key) => ({category: key, detail: value})).value()
       // let newObj = products.map((item, idx) => {
       //     return {
       //         id: item.id,
       //         img: item.image,
       //         rate: Math.floor(item.rating.rate)
       //     }
       // })
        return groupByCategory
    }

    function renderProducts (products) {
        console.log(products)
        let html = ``
        products.map((item,idx)=> {
            html += `<div id="category">
                        <h2>${item.category} total (${item.detail.length})</h2> 
                    </div>
                    <div class="product-list">
                        ${
                            item.detail.map(el => (
                                `
                                    <div class="p-l">
                                        <img src="${el.image}">
                                        <h4 class="title_pro">${el.title}</h2>
                                        <h1>$${el.price}</h2>
                                        <h3>${renderStar(el.rating.rate)}</h2>
                                    </div>
                                `
                            ))
                        }
                    </div>
            
            `
        })
        product.innerHTML = html
    }

    function renderStar(rate) {
        let floor = Math.floor(rate) 
        let text = ''
        for(let i=1;i<=5;i++){
            if(floor <= i) {
                text += '&star;'
            }else{
                text += '&starf;'
            }
        }
        return text
    }

    function addEvent() {
        let posts = document.querySelectorAll("#post")
        for(let i=0;i<posts.length;i++) {
            //console.log(posts[i].getAttribute("key")
            posts[i].addEventListener("click", function(e) {
                let idx=posts[i].getAttribute("key")
                if(document.querySelectorAll("#post-list")[idx].className != "active") {
                    document.querySelectorAll("#post-list")[idx].classList.remove('none')
                    document.querySelectorAll("#post-list")[idx].classList.add('active')
                }else{
                    document.querySelectorAll("#post-list")[idx].classList.remove('active')
                    document.querySelectorAll("#post-list")[idx].classList.add('none')
                }
            })
        }
    }

    async function main() {
        const users = await fetchData('https://jsonplaceholder.typicode.com/users')
        const posts = await fetchData('https://jsonplaceholder.typicode.com/posts')
        const products = await fetchData('https://fakestoreapi.com/products')
        const newUsers = prepareData(users, posts)
        const newProducts = prepareProducts(products)

        renderUsers(newUsers)
        addEvent()
        renderProducts(newProducts)
        //console.log(newUsers)
        //prepareProducts(products)
    }

    main()
    /*

    {
        id: 1,
        name: "Leanne Graham",
        username: "Bret",
        email: "Sincere@april.biz",
        address: {
        street: "Kulas Light",
        suite: "Apt. 556",
        city: "Gwenborough",
        zipcode: "92998-3874",
        geo: {
        lat: "-37.3159",
        lng: "81.1496"
        }
        },
        phone: "1-770-736-8031 x56442",
        website: "hildegard.org",
        company: {
        name: "Romaguera-Crona",
        catchPhrase: "Multi-layered client-server neural-net",
        bs: "harness real-time e-markets"
        }
        }, */
})
